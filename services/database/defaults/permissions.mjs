export default [
  {
    id: 'com.kera.test',
    permissions: {
      node: false,
      root: false,
      media: false,
      geolocation: false,
      pointerLock: false,
      download: false,
      fileSystem: false,
      fullscreen: false,
    },
  },

  {
    id: 'com.kera.wallpaper',
    permissions: {
      node: false,
      root: false,
      media: false,
      geolocation: false,
      pointerLock: false,
      download: false,
      fileSystem: false,
      fullscreen: false,
    },
  },
];
